import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import * as platform from "platform";
declare var GMSServices: any;

import { AppComponent } from "./app.component";

if (platform.isIOS) { 
  GMSServices.provideAPIKey("AIzaSyCv7WylM7NYf0nE6V3FY67bTr0Y9GGBQB0");
}

@NgModule({
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  imports: [NativeScriptModule],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {}
