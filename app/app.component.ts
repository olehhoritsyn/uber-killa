import {Component, ElementRef, ViewChild} from '@angular/core';
import {registerElement} from "nativescript-angular/element-registry";

registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);
 

@Component({
  selector: "my-app",
  template: `
    <GridLayout>
        <MapView (mapReady)="onMapReady($event)"></MapView>
    </GridLayout>
  `
})
export class AppComponent {
  @ViewChild("MapView") mapView: ElementRef;
 
  //Map events
  onMapReady = (event) => {
      console.log("Map Ready");
  };
}
